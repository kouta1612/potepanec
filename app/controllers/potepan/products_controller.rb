class Potepan::ProductsController < ApplicationController
  include Spree::Core::ControllerHelpers::Pricing
  include Spree::Core::ControllerHelpers::Store

  LIMIT_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.friendly.find(params[:id])
    @images = @product.has_variants? ? @product.variant_images : @product.images
    @product_properties = @product.product_properties.includes(:property)
    @related_products = Spree::Product.related_products(@product).limit(LIMIT_NUMBER_OF_RELATED_PRODUCTS)
  end
end

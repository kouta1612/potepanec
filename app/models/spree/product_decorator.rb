Spree::Product.class_eval do
  scope :related_products, -> (product) do
    includes(master: [:default_price, :images]).in_taxons(product.taxons).where.not(id: product).distinct
  end
end

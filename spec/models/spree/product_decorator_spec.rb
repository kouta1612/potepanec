require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#related_products" do
    let!(:taxon1) { create(:taxon, name: "brand") }
    let!(:taxon2) { create(:taxon, name: "clothing") }
    let!(:taxon3) { create(:taxon, name: "book") }
    let!(:product1) { create(:product, name: "rails_shirt", taxons: [taxon1, taxon2]) }
    let!(:product2) { create(:product, name: "apatch_shirt", taxons: [taxon1, taxon2]) }
    let!(:product3) { create(:product, name: "t_shirt", taxons: [taxon2]) }
    let!(:product4) { create(:product, name: "harry_potter", taxons: [taxon3]) }

    it "returns correct related_products with duplication" do
      expect(Spree::Product.related_products(product1)).to match_array [product2, product3]
    end

    it "returns empty collection" do
      expect(Spree::Product.related_products(product4)).to be_empty
    end
  end
end

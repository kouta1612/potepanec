require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  given!(:taxon1) { create(:taxon, name: "brand") }
  given!(:taxon2) { create(:taxon, name: "clothing") }
  given!(:product1) { create(:product, name: "rails_shirt", price: 13.99, taxons: [taxon1, taxon2]) }
  given!(:product2) { create(:product, name: "apatch_shirt", price: 14.99, taxons: [taxon1, taxon2]) }
  given!(:product3) { create(:product, name: "t_shirt", price: 15.99, taxons: [taxon2]) }
  given!(:product_property) { create(:product_property, product: product1) }

  background do
    visit potepan_product_path(product1)
  end
  scenario "show correct product depending on product_id" do
    expect(page).to have_selector '.page-title', text: product1.name
    expect(page).to have_selector '.breadcrumb', text: product1.name
    expect(page).to have_selector '.media-body', text: product1.name
    expect(page).to have_selector '.media-body', text: product1.display_price
  end

  scenario "show correct related product" do
    expect(page).not_to have_selector '.productCaption', text: product1.name
    expect(page).not_to have_selector '.productCaption', text: product1.display_price
    expect(page).to have_selector '.productCaption', text: product2.name
    expect(page).to have_selector '.productCaption', text: product2.display_price
    expect(page).to have_selector '.productCaption', text: product3.name
    expect(page).to have_selector '.productCaption', text: product3.display_price
  end

  scenario "show correct product page after click product name" do
    click_link product2.name, href: potepan_product_path(product2)
    expect(current_path).to eq potepan_product_path(product2)
    expect(page).to have_selector '.media-body', text: product2.name
    expect(page).to have_selector '.media-body', text: product2.display_price
    expect(page).to have_selector '.productCaption', text: product1.name
    expect(page).to have_selector '.productCaption', text: product1.display_price
    expect(page).not_to have_selector '.productCaption', text: product2.name
    expect(page).not_to have_selector '.productCaption', text: product2.display_price
    expect(page).to have_selector '.productCaption', text: product3.name
    expect(page).to have_selector '.productCaption', text: product3.display_price
  end
end

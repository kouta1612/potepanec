require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: "categories") }
  given!(:taxon1) { create(:taxon, name: "bags", parent: taxonomy.root, taxonomy: taxonomy) }
  given!(:taxon2) { create(:taxon, name: "clothing", parent: taxonomy.root, taxonomy: taxonomy) }
  given!(:child1) { create(:taxon, name: "shirts", parent: taxon2, taxonomy: taxonomy) }
  given!(:child2) { create(:taxon, name: "pants", parent: taxon2, taxonomy: taxonomy) }
  given!(:grand_child) { create(:taxon, name: "boxer_pants", parent: child2, taxonomy: taxonomy) }
  given!(:product1) { create(:product, taxons: [taxon1], name: "bag", price: 13.99) }
  given!(:product2) { create(:product, taxons: [child1], name: "shirt", price: 14.99) }
  given!(:product3) { create(:product, taxons: [child1], name: "t_shirt", price: 15.99) }

  scenario "show correct products depending on category_id" do
    visit potepan_category_path(taxon2.id)
    expect(page).to have_selector '.page-title', text: taxon2.name
    expect(page).to have_selector '.breadcrumb', text: taxon2.name
    expect(page).to have_selector '.side-nav', text: taxonomy.name
    expect(page).to have_selector '.side-nav', text: "#{taxon1.name} (#{taxon1.all_products.count})"
    expect(page).to have_selector '.side-nav', text: "#{child1.name} (#{child1.all_products.count})"
    expect(page).to have_selector '.side-nav', text: taxon2.name
    expect(page).not_to have_selector '.side-nav', text: grand_child.name
    expect(page).not_to have_selector '.productCaption', text: product1.name
    expect(page).not_to have_selector '.productCaption', text: product1.display_price
    expect(page).to have_selector '.productCaption', text: product2.name
    expect(page).to have_selector '.productCaption', text: product2.display_price
    expect(page).to have_selector '.productCaption', text: product3.name
    expect(page).to have_selector '.productCaption', text: product3.display_price
  end

  scenario "show correct products after click category_link" do
    visit potepan_category_path(taxon2.id)
    click_link taxonomy.name
    click_link taxon1.name
    expect(page).to have_selector '.productCaption', text: product1.name
    expect(page).to have_selector '.productCaption', text: product1.display_price
    expect(page).not_to have_selector '.productCaption', text: product2.name
    expect(page).not_to have_selector '.productCaption', text: product2.display_price
    expect(page).not_to have_selector '.productCaption', text: product3.name
    expect(page).not_to have_selector '.productCaption', text: product3.display_price
  end

  scenario "show correct product_page after click product_link" do
    visit potepan_category_path(taxon2.id)
    click_link product2.name, href: potepan_product_path(product2)
    expect(current_path).to eq potepan_product_path(product2)
    expect(page).to have_selector '.page-title', text: product2.name
    expect(page).to have_selector '.breadcrumb', text: product2.name
    expect(page).to have_selector '.media-body', text: product2.name
    expect(page).to have_selector '.media-body', text: product2.display_price
  end

  scenario "show correct category and product page after click link" do
    visit potepan_category_path(taxon2.id)
    click_link product2.name, href: potepan_product_path(product2)
    expect(current_path).to eq potepan_product_path(product2)
    click_link "一覧ページへ戻る", href: potepan_category_path(taxon2.id)
    expect(current_path).to eq potepan_category_path(taxon2.id)
  end
end

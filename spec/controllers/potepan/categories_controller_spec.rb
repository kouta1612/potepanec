require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:taxonomy) { create(:taxonomy, name: "categories") }
    let!(:taxon1) { create(:taxon, name: "bags", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:taxon2) { create(:taxon, name: "clothing", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product1) { create(:product, name: "bag", taxons: [taxon1]) }
    let!(:product2) { create(:product, name: "shirt", taxons: [taxon2]) }

    before do
      get :show, params: { id: taxon1.id }
    end

    it "responds succsess" do
      expect(response).to be_successful
    end

    it "returns http 200" do
      expect(response).to have_http_status 200
    end

    it "returns show template" do
      expect(response).to render_template :show
    end

    it "assigns correct taxonomy" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "assigns correct taxon" do
      expect(assigns(:taxon)).to eq taxon1
    end

    it "assigns correct products" do
      expect(assigns(:products)).to match_array product1
    end
  end
end

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:taxon1) { create(:taxon, name: "brand") }
    let!(:taxon2) { create(:taxon, name: "clothing") }
    let!(:taxon3) { create(:taxon, name: "book") }
    let!(:product1) { create(:product, name: "rails_shirt", taxons: [taxon1, taxon2]) }
    let!(:product2) { create(:product, name: "apatch_shirt", taxons: [taxon1, taxon2]) }
    let!(:product3) { create(:product, name: "t_shirt", taxons: [taxon2]) }
    let!(:product4) { create(:product, name: "harry_potter", taxons: [taxon3]) }
    let!(:product5) { create(:product, name: "apatch", taxons: [taxon1]) }
    let!(:product6) { create(:product, name: "jeans", taxons: [taxon2]) }
    let!(:product7) { create(:product, name: "rails", taxons: [taxon1]) }
    let!(:product_property) { create(:product_property, product: product1) }

    context "request with product id" do
      before do
        get :show, params: { id: product1.id }
      end

      it "responds success" do
        expect(response).to be_successful
      end

      it "returns http 200" do
        expect(response).to have_http_status 200
      end

      it "returns show template" do
        expect(response).to render_template :show
      end

      it "assigns correct product" do
        expect(assigns(:product)).to eq product1
      end

      it "assigns correct product_properties" do
        expect(assigns(:product_properties)).to match_array product_property
      end

      it "returns correct number of related_products with duplication" do
        expect(assigns(:related_products).size).to eq 4
      end
    end

    context "request with product friendly_id" do
      it "responds success" do
        get :show, params: { id: product1 }
        expect(response).to be_successful
      end
    end
  end
end
